<?php
/**
 * Description of class
 *
 * @author croydon
 */
class ISPConfigDebian10OS extends ISPConfigDebianOS {
	protected function updateMySQLConfig($mysql_root_pw) {
		ISPConfigLog::info('Writing MySQL config files.', true);
		$this->replaceContents('/etc/mysql/debian.cnf', array('/^password\s*=.*$/m' => 'password = ' . $mysql_root_pw));
		$this->replaceContents('/etc/mysql/mariadb.conf.d/50-server.cnf', array('/^bind-address/m' => '#bind-address'), true, 'mysqld');
	}

	public function getRestartServiceCommand($service, $command = 'restart') {
		if($command != 'start' && $command != 'stop' && $command != 'status') {
			$command = 'restart';
		}

		switch($service) {
			case 'mysql':
			case 'mariadb':
				$service = 'mariadb';
				break;
			case 'pureftpd':
				$service = 'pure-ftpd-mysql';
				break;
		}

		return 'systemctl ' . $command . ' ' . escapeshellarg($service) . ' 2>&1';
	}

	protected function getPackagesToInstall($section) {
		$packages = parent::getPackagesToInstall($section);

		if($section === 'mail') {
			$packages[] = 'p7zip';
			$packages[] = 'p7zip-full';
			$packages[] = 'unrar-free';
			$packages[] = 'lrzip';
		} elseif($section === 'base') {
			//$packages[] = 'jailkit';
		}

		return $packages;
	}

	protected function shallCompileJailkit() {
		return true;
	}

	protected function setDefaultPHP() {
		ISPConfigLog::info('Setting default system php version.', true);
		$cmd = 'update-alternatives --set php /usr/bin/php7.3';
		$result = $this->exec($cmd);
		if($result === false) {
			throw new ISPConfigOSException('Command ' . $cmd . ' failed.');
		}

		if(ISPConfig::shallInstall('web')) {
			// When --use-php-system is used, there is no alternative for php-fpm.sock.
			if(ISPConfig::wantsPHP() === 'system') {
				$cmd = 'update-alternatives --set php-cgi /usr/bin/php-cgi7.3';
			} else {
				$cmd = 'update-alternatives --set php-cgi /usr/bin/php-cgi7.3 ; update-alternatives --set php-fpm.sock /run/php/php7.3-fpm.sock';
			}
			$result = $this->exec($cmd);
			if($result === false) {
				throw new ISPConfigOSException('Command ' . $cmd . ' failed.');
			}
		}
	}

	protected function getRoundcubePackages() {
		return array(
			'roundcube/buster-backports',
			'roundcube-core/buster-backports',
			'roundcube-mysql/buster-backports',
			'roundcube-plugins/buster-backports'
		);
	}

	protected function addBusterBackportsRepo() {
		ISPConfigLog::info('Activating Buster Backports repository.', true);
		$cmd = 'echo "deb http://deb.debian.org/debian buster-backports main" > /etc/apt/sources.list.d/buster-backports.list';
		$result = $this->exec($cmd);
		if($result === false) {
			throw new ISPConfigOSException('Command ' . $cmd . ' failed.');
		}
	}

	protected function installRoundcube($mysql_root_pw) {
		$this->addBusterBackportsRepo();
		$this->updatePackageList();
		parent::installRoundcube($mysql_root_pw);
	}

	protected function getFail2BanJail() {
		$jk_jail = '[pure-ftpd]
enabled = true
port = ftp
filter = pure-ftpd
logpath = /var/log/syslog
maxretry = 3

[dovecot]
enabled = true
filter = dovecot
logpath = /var/log/mail.log
maxretry = 5

[postfix-sasl]
enabled = true
port = smtp
filter = postfix[mode=auth]
logpath = /var/log/mail.log
maxretry = 3';
		return $jk_jail;
	}

	protected function installPHPMyAdmin($mysql_root_pw) {
		if(!ISPConfig::shallInstall('web') || !ISPConfig::shallInstall('pma')) {
			return;
		}

		ISPConfigLog::info('Installing package phpmyadmin', true);

		if(!is_dir('/usr/share/phpmyadmin')) {
			mkdir('/usr/share/phpmyadmin', 0755, true);
		}
		if(!is_dir('/etc/phpmyadmin')) {
			mkdir('/etc/phpmyadmin', 0755);
		}
		if(!is_dir('/var/lib/phpmyadmin/tmp')) {
			mkdir('/var/lib/phpmyadmin/tmp', 0777, true);
		}
		touch('/etc/phpmyadmin/htpasswd.setup');

		$cmd = 'chown -R www-data:www-data ' . escapeshellarg('/var/lib/phpmyadmin') . ' ; cd /tmp ; rm -f phpMyAdmin-4.9.0.1-all-languages.tar.gz ; wget "https://files.phpmyadmin.net/phpMyAdmin/4.9.0.1/phpMyAdmin-4.9.0.1-all-languages.tar.gz" 2>/dev/null && tar xfz phpMyAdmin-4.9.0.1-all-languages.tar.gz && cp -a phpMyAdmin-4.9.0.1-all-languages/* /usr/share/phpmyadmin/ && rm -f phpMyAdmin-4.9.0.1-all-languages.tar.gz && rm -rf phpMyAdmin-4.9.0.1-all-languages';
		$result = $this->exec($cmd);
		if($result === false) {
			throw new ISPConfigOSException('Command ' . $cmd . ' failed.');
		}

		copy('/usr/share/phpmyadmin/config.sample.inc.php', '/usr/share/phpmyadmin/config.inc.php');

		$replacements = array(
			'/^(?:\s*\/\/)?\s*\$cfg\[\'blowfish_secret\'\]\s*=.*$/m' => '$cfg[\'blowfish_secret\'] = \'' . substr(sha1(uniqid('pre', true)), 0, 32) . '\';',
			'/^(?:\s*\/\/)?\s*\$cfg\[\'TempDir\'\]\s*=.*$/m' => '$cfg[\'TempDir\'] = \'/var/lib/phpmyadmin/tmp\';'
		);
		$this->replaceContents('/usr/share/phpmyadmin/config.inc.php', $replacements, true);

		$contents = '# phpMyAdmin default Apache configuration

Alias /phpmyadmin /usr/share/phpmyadmin

<Directory /usr/share/phpmyadmin>
 Options FollowSymLinks
 DirectoryIndex index.php

 <IfModule mod_php7.c>
 AddType application/x-httpd-php .php

 php_flag magic_quotes_gpc Off
 php_flag track_vars On
 php_flag register_globals Off
 php_value include_path .
 </IfModule>

</Directory>

# Authorize for setup
<Directory /usr/share/phpmyadmin/setup>
 <IfModule mod_authn_file.c>
 AuthType Basic
 AuthName "phpMyAdmin Setup"
 AuthUserFile /etc/phpmyadmin/htpasswd.setup
 </IfModule>
 Require valid-user
</Directory>

# Disallow web access to directories that don\'t need it
<Directory /usr/share/phpmyadmin/libraries>
 Order Deny,Allow
 Deny from All
</Directory>
<Directory /usr/share/phpmyadmin/setup/lib>
 Order Deny,Allow
 Deny from All
</Directory>';
		if(ISPConfig::$WEBSERVER === ISPC_WEBSERVER_APACHE) {
			file_put_contents('/etc/apache2/conf-available/phpmyadmin.conf', $contents);

			$cmd = 'a2enconf phpmyadmin';
			$result = $this->exec($cmd);
			if($result === false) {
				throw new ISPConfigOSException('Command ' . $cmd . ' failed.');
			}

			$this->restartService('apache2');
		}

		$pma_pass = ISPConfigFunctions::generatePassword(15);
		$pma_pass_enc = preg_replace('/[\'\\\\]/', '\\$1', $pma_pass);

		$queries = array(
			'CREATE DATABASE phpmyadmin;',
			'CREATE USER \'pma\'@\'localhost\' IDENTIFIED BY \'' . $pma_pass_enc . '\';',
			'GRANT ALL PRIVILEGES ON phpmyadmin.* TO \'pma\'@\'localhost\' IDENTIFIED BY \'' . $pma_pass_enc . '\' WITH GRANT OPTION;',
			'FLUSH PRIVILEGES;'
		);

		foreach($queries as $query) {
			$cmd = 'mysql --defaults-file=/etc/mysql/debian.cnf -e ' . escapeshellarg($query) . ' 2>&1';
			$result = $this->exec($cmd);
			if($result === false) {
				ISPConfigLog::warn('Query ' . $query . ' failed.', true);
			}
		}

		$cmd = 'mysql --defaults-file=/etc/mysql/debian.cnf -D phpmyadmin < /usr/share/phpmyadmin/sql/create_tables.sql';
		$result = $this->exec($cmd);
		if($result === false) {
			ISPConfigLog::warn('Command ' . $cmd . ' failed.', true);
		}

		$uncomment = array(
			array(
				'first_line' => '/^(?:\s*\/\/)?\s*\$cfg\[\'Servers\'\]\[\$i\]/',
				'last_line' => '/####nomatch###/',
				'search' => '/^(?:\s*\/\/)?\s*\$cfg\[\'Servers\'\]\[\$i\]/'
			)
		);
		$this->uncommentLines('/usr/share/phpmyadmin/config.inc.php', $uncomment, '//');

		$replacements = array(
			'/^(?:\s*\/\/)?\s*(\$cfg\[\'Servers\'\]\[\$i\]\[\'controlhost\'\])\s*=.*$/m' => '$1 = \'localhost\';',
			'/^(?:\s*\/\/)?\s*(\$cfg\[\'Servers\'\]\[\$i\]\[\'controlport\'\])\s*=.*$/m' => '$1 = \'\';',
			'/^(?:\s*\/\/)?\s*(\$cfg\[\'Servers\'\]\[\$i\]\[\'controluser\'\])\s*=.*$/m' => '$1 = \'pma\';',
			'/^(?:\s*\/\/)?\s*(\$cfg\[\'Servers\'\]\[\$i\]\[\'controlpass\'\])\s*=.*$/m' => '$1 = \'' . $pma_pass_enc . '\';',
		);
		$this->replaceContents('/usr/share/phpmyadmin/config.inc.php', $replacements, false);
	}

	protected function getSystemPHPVersion() {
		return '7.3';
	}
}
